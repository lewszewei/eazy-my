import React from 'react';
import { connect } from "react-redux";

const Lists = ({products}) => {
    return (
        <div className="page">
            <div className="pageName">LISTING</div>
            <div className="productList">
            {products.map(product =>
                <div className="product" key={product.id}>
                    <div className="productImage"><img src={product.attributes.links.image}/></div>
                    <div className="productTitle">{product.attributes.title}</div>
                    <div className="productPrice">{product.attributes.price}</div>
                </div>
            )}
            </div>
        </div>
    )
}

const mapStateToProps = state => {
  return {
    products: state.products
  }
};

export default connect(mapStateToProps)(Lists);
