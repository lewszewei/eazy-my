import axios from 'axios';

const apiUrl = 'http://5b35ede16005b00014c5dc86.mockapi.io/list';

export const showProduct = id => ({
  type: 'SHOW_PRODUCT',
  id
})

export const fetchProductsSuccess = (products) => {
  return {
    type: 'FETCH_PRODUCTS',
    products
  }
};

export const fetchProducts = () => {
  return (dispatch) => {
    return axios.get(apiUrl)
      .then(response => {
        dispatch(fetchProductsSuccess(response.data.data))
      })
      .catch(error => {
        throw(error);
      });
  };
};
