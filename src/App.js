import React from 'react';
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Lists from './Lists'

const App = ({store}) => {
    return (
        <Provider store={store}>
            <div className="navbar-container">
                <div className="navbar">
                    <div className="navbar-logo"><img src="logo.png"/></div>
                    <div className="navbar-menu">
                        <ul>
                            <li><a href="#">Catergories</a></li>
                            <li><a href="#">Notification</a></li>
                            <li><a href="#">Login/Sign up</a></li>
                            <li><a href="#">Help</a></li>
                        </ul>
                    </div>
                    <div className="icon">
                        <a href="javascript:void(0);">
                            <i className="fa fa-bars"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div className="container">
                <Router>
                    <Route path="/" component={Lists}/>
                </Router>
            </div>
        </Provider>
    )
}

export default App;
