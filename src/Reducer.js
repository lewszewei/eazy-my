import { combineReducers } from 'redux';

const currentProduct = (state = "", action) => {
  switch (action.type) {
    case 'SHOW_PRODUCT':
      return action.id
    default:
      return state
  }
}

const products = (state = [], action) => {
  switch (action.type) {
    case 'FETCH_PRODUCTS':
      return action.products;
    default:
      return state;
  }
}

export default combineReducers({
    currentProduct,
    products
})

